app.factory("msgApiHandler", ["$http", ($http) => {
    function sendMsg(message, userData){

        let date = new Date();
        let time = date.getTime();

        let messageJSON = {
            "name" : userData.username,
            "message" : message,
            "time" : time,
            "style" : {
                "color" : userData.color,
                "background-color" : userData.bgColor
            }
        };

        return new Promise((resolve, reject) => {
            $http({
                "method" : "POST",
                "url" : "/sendMessage",
                "data" : messageJSON
            }).then(function(response){
                var chatJSON = response.data;
                
                resolve(chatJSON);
            }).catch((response) => {
                reject(response);
            });
        });
    }

    function getMsg(){
        return new Promise((resolve, reject) => {
            $http({
                "method" : "GET",
                "url" : "/getMessages"
            }).then(function(response){
                var chatJSON = response.data;

                resolve(chatJSON);
            });
        });
    }

    return {
        sendMsg : sendMsg,
        getMsg : getMsg
    }
}]);