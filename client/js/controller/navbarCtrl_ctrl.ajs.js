app.controller("navbarCtrl", ["$scope", function ($scope) {
    let userInfo = {};

    function renderUserInfo(){
        $scope.userShort = userInfo.username[0];
        $scope.userStyle = {
            "background-color" : userInfo.bgColor,
            "color" : userInfo.color
        }
    }
    
    $scope.$on("setUserInfo", (event, args) => {
        userInfo = args[0];

        renderUserInfo();
    });
}]);