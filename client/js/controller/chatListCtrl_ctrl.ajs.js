app.controller('chatListCtrl', ["$scope", "msgApiHandler", "$timeout",
                    function ($scope, msgApiHandler, $timeout) {

    function adjustScrolling(){
        let element = document.querySelector("html");
        element.scrollTop = element.scrollHeight;
    }

    function shouldScroll(){
        let element = document.querySelector("html");
        
        if(element.scrollTop != element.scrollHeight){
            return false;
        }

        return true;
    }

    function manageNewMessages(chatJSON){
        $scope.$apply(() => {
            let doAdjustScrolling = shouldScroll();

            $scope.chat = chatJSON;

            if(doAdjustScrolling){
                $timeout(() => {
                    adjustScrolling(oldScrollHeight);
                });
            }
        });
    }

    function getMessages(){
        msgApiHandler.getMsg().then(chatJSON => {
            manageNewMessages(chatJSON);
        });
    }

    $scope.$on("newMessages", (event, args) => {
        manageNewMessages(args[0]);
    });
    
    // GET MESSAGES INITIALY
    $timeout(getMessages);
}]);