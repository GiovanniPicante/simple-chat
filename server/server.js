const express = require("express");
const bodyParser = require("body-parser");
const path = require('path');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

app.use(bodyParser.json());

const ClientPath = path.join(__dirname, "../client");
const DependenciesPath = path.join(__dirname, "../node_modules");

app.use('/src', express.static(ClientPath));
app.use('/dep', express.static(DependenciesPath));


var chatJSON = [
    {
        "name" : "SERVER",
        "message" : `<b>Willkommen im Simple Chat!</b> <br>
                    Wenn du noch nicht da warst, fülle bitte das Formular aus und verschicke deine erste Nachricht. <br>
                    Falls du deinen Namen oder Farbe ändern möchtest, sende einfach eine Nachricht "clear userdata". <br>
                    <br>
                    Viel Spaß!`,
        "time" : new Date().getTime(),
        "style" : {
            "background-color" : "darkblue",
            "color": "yellow"
        }
    }
];

function receiveMessage(messageJSON){
    return new Promise(resolve => {
        chatJSON.push(messageJSON);
        chatJSON = chatJSON.reverse().slice(0, 20).reverse();

        resolve(chatJSON);
    });
}

function broadcastChat(chatJSON){
    io.emit("new messages", chatJSON);
}

function handleIncommingMessage(messageJSON){
    let messageReceived = receiveMessage(messageJSON)
    messageReceived.then(broadcastChat);
}

io.on('connection', function(socket){    
    socket.on("send message", handleIncommingMessage);
});


app.get('/', function(req, res) {
    res.sendFile(path.join(ClientPath, '/index.html'));
});

app.post("/sendMessage", function(request, response){
    receiveMessage(request.body).then(chatJSON => {
        response.send(chatJSON);
    });
});

app.get("/getMessages", function(request, response){
    response.send(chatJSON);
});

new Promise(resolve => {
    http.listen(8080, resolve);
}).then(() => {
    console.log("Server runs on port 8080");
});
