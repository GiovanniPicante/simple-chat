app.controller('inputCtrl', ["$scope", "msgSocketHandler", "$timeout", "$filter", "$rootScope",
                function ($scope, msgSocketHandler, $timeout, $filter, $rootScope) {
    let userData;

    $scope.userData = {
        "username" : "",
        "color" : "",
        "bgColor" : "",
    }
    
    $scope.readyForChat = false;

    jQuery("#messageInput").keypress(function (e) {
        if(e.which == 13 && !e.shiftKey) {
            e.preventDefault();

            jQuery("#sendButton").click();
        }
    });

    $scope.checkBackspace = function (e) {
        if(e.which == 8) {
            if(jQuery("#messageInput").val().length == 0){
                $scope.messageText = "";
            }
        }
    };
    
    $scope.sendMessage = function(){
        if(!$scope.readyForChat){
            userData = $scope.userData;

            localStorage.setItem("userData", JSON.stringify($scope.userData));
            $scope.$broadcast("setUserInfo", [userData]);

            $scope.readyForChat = true;
        }

        var message = $scope.messageText + " ";
        $scope.messageText = "";

        message = $filter("replaceLineBreak")(message);
        message = $filter("detectLink")(message);

        if(message == "clear userdata "){
            localStorage.removeItem("userData");
            
            window.location.reload();

            return;
        }

        msgSocketHandler.sendMsg(message, userData).then((chatJSON) => {
            addEvents();
        });
    }

    function init(){
        let userDataString = localStorage.getItem("userData");

        if(userDataString !== undefined && userDataString != null && userDataString != "{}"){
            userData = JSON.parse(userDataString);

            $rootScope.$broadcast("setUserInfo", [userData]);
            
            $scope.readyForChat = true;
        }
    };

    $timeout(init);
}]);