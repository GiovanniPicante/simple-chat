app.filter('replaceLineBreak', function() {
    return input => {
        if(input === undefined){
            return;
        }

        return input.replace(/(?:\r\n|\r|\n)/g, '<br>')
    }
});