app.factory("msgSocketHandler", ["$rootScope", ($rootScope) => {
    const socket = io();

    function sendMsg(message, userData){
        let date = new Date();
        let time = date.getTime();

        let messageJSON = {
            "name" : userData.username,
            "message" : message,
            "time" : time,
            "style" : {
                "color" : userData.color,
                "background-color" : userData.bgColor
            }
        };

        return new Promise((resolve, reject) => {
            socket.emit("send message", messageJSON);
            resolve();
        });
    }

    socket.on("new messages", chatJSON => {
        $rootScope.$broadcast("newMessages", [chatJSON]);
    });

    return {
        sendMsg : sendMsg
    }
}]);